import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://simon.inder.gov.co/registro')

WebUI.click(findTestObject('Objetos SimonInder/lst_TipoPersona'))

WebUI.click(findTestObject('Objetos SimonInder/btn_PersonaNatural'))

WebUI.click(findTestObject('Objetos SimonInder/lst_TipoDeDocumento'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Cedula'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Identificacion'), '1934678945')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Nombres'), 'Alejandra')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Apellidos'), 'Blandon Rios')

WebUI.click(findTestObject('Objetos SimonInder/lst_Genero'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Femenino'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_FechaNacimiento'), '05121994')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Clave'), 'aleja1234')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_ConfirmacionClave'), 'aleja1234')

WebUI.click(findTestObject('Objetos SimonInder/lst_Municipio'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Municipio'), 'Medellin')

WebUI.click(findTestObject('Objetos SimonInder/btn_Medellin'))

WebUI.click(findTestObject('Objetos SimonInder/lst_Estrato'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Estrato'), '3')

WebUI.click(findTestObject('Objetos SimonInder/btn_Estrato3'))

WebUI.click(findTestObject('Objetos SimonInder/rbt_Barrio'))

WebUI.click(findTestObject('Objetos SimonInder/lst_Calle'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Calle'), 'Calle')

WebUI.click(findTestObject('Objetos SimonInder/btn_Calle'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroCalle'), '30')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroPuerta1'), '30')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_NumeroPuerta2'), '30')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Correo'), 'alejandrablandon@hotmail.com')

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_Telefono'), '3146721723')

WebUI.click(findTestObject('Objetos SimonInder/cbx_Politicas'))

WebUI.click(findTestObject('Objetos SimonInder/cbx_Terminos'))

WebUI.delay(20)

WebUI.click(findTestObject('Objetos SimonInder/lst_Barrios'))

WebUI.sendKeys(findTestObject('Objetos SimonInder/cmp_BarrioPoblado'), 'Aguacatala')

WebUI.click(findTestObject('Objetos SimonInder/btn_BarrioPoblado'))

WebUI.click(findTestObject('Objetos SimonInder/btn_Guardar'))

WebUI.delay(10)

WebUI.closeBrowser()

