<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en el género Femenino</description>
   <name>btn_Femenino</name>
   <tag></tag>
   <elementGuidId>06fa5fc2-ac04-4224-8098-726f13817b4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'select2-results-dept-0 select2-result select2-result-selectable select2-highlighted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results-dept-0 select2-result select2-result-selectable select2-highlighted</value>
   </webElementProperties>
</WebElementEntity>
