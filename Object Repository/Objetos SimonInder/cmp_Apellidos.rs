<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresan los apellidos</description>
   <name>cmp_Apellidos</name>
   <tag></tag>
   <elementGuidId>f4e18901-effb-4180-85f0-27fc61d22167</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'apellidos']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>apellidos</value>
   </webElementProperties>
</WebElementEntity>
