<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en el botón de Barrio el Poblado</description>
   <name>btn_BarrioPoblado</name>
   <tag></tag>
   <elementGuidId>d2eca514-23a4-494b-9dfd-67bb9a248923</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'select2-results-dept-0 select2-result select2-result-selectable select2-highlighted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results-dept-0 select2-result select2-result-selectable select2-highlighted</value>
   </webElementProperties>
</WebElementEntity>
