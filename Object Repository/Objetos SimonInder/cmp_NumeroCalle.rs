<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Se ingresa el número de la calle correspondiente</description>
   <name>cmp_NumeroCalle</name>
   <tag></tag>
   <elementGuidId>b8e2a74f-992c-4155-95c2-94d31ebc4b50</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'formulario_registro_direccion_format_numero_via']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>formulario_registro_direccion_format_numero_via</value>
   </webElementProperties>
</WebElementEntity>
