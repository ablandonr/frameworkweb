<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Botón para seleccionar el tipo de documento</description>
   <name>lst_TipoDeDocumento</name>
   <tag></tag>
   <elementGuidId>dbb3dfd0-2fa8-4a99-be6e-3026eece0d97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_tipoidentificacion']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_tipoidentificacion</value>
   </webElementProperties>
</WebElementEntity>
