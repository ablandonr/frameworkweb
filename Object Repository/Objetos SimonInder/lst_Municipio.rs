<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click para desplegar los municipios disponibles</description>
   <name>lst_Municipio</name>
   <tag></tag>
   <elementGuidId>a1ff6712-d5ad-4774-bd48-f994dad82959</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_municipio']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_municipio</value>
   </webElementProperties>
</WebElementEntity>
