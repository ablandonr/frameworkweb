<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Boton para elegir la Cédula de Ciudadania</description>
   <name>btn_Cedula</name>
   <tag></tag>
   <elementGuidId>5c49c5a2-250d-486d-8fb9-4f5c8aabfb68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'select2-results-dept-0 select2-result select2-result-selectable select2-highlighted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results-dept-0 select2-result select2-result-selectable select2-highlighted</value>
   </webElementProperties>
</WebElementEntity>
