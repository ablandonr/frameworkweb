<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Botón para seleccionar una persona natural</description>
   <name>btn_PersonaNatural</name>
   <tag></tag>
   <elementGuidId>c764093c-f6d1-4cfb-b821-88db6d172c81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'select2-results-dept-0 select2-result select2-result-selectable select2-highlighted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results-dept-0 select2-result select2-result-selectable select2-highlighted</value>
   </webElementProperties>
</WebElementEntity>
