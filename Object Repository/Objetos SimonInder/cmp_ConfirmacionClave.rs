<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se escribe por segunda vez la clave para confirmar el dato</description>
   <name>cmp_ConfirmacionClave</name>
   <tag></tag>
   <elementGuidId>2333e240-b46f-417e-b238-658b9b2b84d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'clave_dos']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>clave_dos</value>
   </webElementProperties>
</WebElementEntity>
