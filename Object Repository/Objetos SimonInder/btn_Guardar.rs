<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click en el botón de Guardar</description>
   <name>btn_Guardar</name>
   <tag></tag>
   <elementGuidId>bba26fd4-a7e7-4504-ba57-c7ced434882e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'registro_save']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>registro_save</value>
   </webElementProperties>
</WebElementEntity>
