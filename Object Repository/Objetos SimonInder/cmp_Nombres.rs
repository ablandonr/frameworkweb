<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Campo en el que se ingresa el Nombre completo</description>
   <name>cmp_Nombres</name>
   <tag></tag>
   <elementGuidId>12867f44-a9b6-4a78-9002-0c38dd12103c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'nombres']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>nombres</value>
   </webElementProperties>
</WebElementEntity>
